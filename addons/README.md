Will contain some addons configuration

For example db addon will contain database settings and in server.js we will call just one function

Example structure:
 * **addons**
    * db.js
    * passport.js
    * socket.js
    * socketInject.js
