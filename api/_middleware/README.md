Will contain some useful middleware, that used in all routers

Example structure:
 * **_middleware**
    * respond.js
    * auth.js
    * role.js
    * validate.js
