Will contain folders with routers, controllers, validators, services and files with middleware

Services will call repositories

Validators will be used a router middleware

Example structure:
 * **api**
    * **app-name**
        * *app-name*.service.js
        * *app-name*.router.js
        * *app-name*.validators.js
        * *app-name*.controllers.js

    * **_middleware**
        * respond.js
        * auth.js
        * role.js
        * validate.js
