An example folder to show project structure. Replace app-name with your router name (e.g. users, equipment, rent, bar, beach, ...)

Here will be router data

Example structure:
 * **users**
    * users.service.js
    * users.router.js
    * users.validators.js
    * users.controllers.js

 * **equipment**
    * equipment.service.js
    * equipment.router.js
    * equipment.validators.js
    * equipment.controllers.js

 * **bar-menu**
    * bar-menu.service.js
    * bar-menu.router.js
    * bar-menu.validators.js
    * bar-menu.controllers.js

 * **bar-ingredient**
    * bar-ingredient.service.js
    * bar-ingredient.router.js
    * bar-ingredient.validators.js
    * bar-ingredient.controllers.js

 * **abonement**
    * abonement.service.js
    * abonement.router.js
    * abonement.validators.js
    * abonement.controllers.js

 * **lake-route**
    * lake-route.service.js
    * lake-route.router.js
    * lake-route.validators.js
    * lake-route.controllers.js

 * **achievement**
    * achevement.service.js
    * achevement.router.js
    * achevement.validators.js
    * achevement.controllers.js

 * **rent-sale-equipment**
    * rent-sale-equipment.service.js
    * rent-sale-equipment.router.js
    * rent-sale-equipment.validators.js
    * rent-sale-equipment.controllers.js

 * **service-for-clients**
    * service-for-clients.service.js
    * service-for-clients.router.js
    * service-for-clients.validators.js
    * service-for-clients.controllers.js

 * **invoice**
    * invoice.service.js
    * invoice.router.js
    * invoice.validators.js
    * invoice.controllers.js

 * **beach**
    * beach.service.js
    * beach.router.js
    * beach.validators.js
    * beach.controllers.js

 * **blog**
    * blog.service.js
    * blog.router.js
    * blog.validators.js
    * blog.controllers.js

 * **promotion**
    * promotion.service.js
    * promotion.router.js
    * promotion.validators.js
    * promotion.controllers.js

 * **payment**
    * payment.service.js
    * payment.router.js
    * payment.validators.js
    * payment.controllers.js
