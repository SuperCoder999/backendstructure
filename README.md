Project will use: MongoDB, mongoose, express, @hapi/joi, celebrate, JS, Microservices architecture

Protocols: HTTP, WebSocket

MongoDB is used, because it doesn't require migrations, seeds, associations, etc.

Microservices architecture is used, because repositories, helpers, addons, etc. are fully reusable

WebSocket will be used to show equipment, services, etc. statuses

HTTP will be used to make other requests

There will be:
 * Models
 * Repositories
 * Services
 * Controllers
 * Validators
 * Routers
 * Middlewares
 * Addons
 * Helpers
 * Configurations

[Structure Schema](https://drive.google.com/file/d/1cFAS9K6BnaWL7tZ8lgkr9XlT2aC5ZY_o/view?usp=sharing)

There are some 'apps' in the 'api' folder.

Each 'app' folder contains:
 * service
 * controller(s)
 * validator(s)
 * router

And 'app' folder uses one or more repositories.

Repositories and models are in the 'data' folder.

Configuration files are in the 'config' folder.

## Models
Each model describes one collection in the database (MongoDB)

## Repositories
Repositories call Model methods to get data, that will be sent to the client

## Services
Services call Repositories and Helpers and provide required business tasks

## Helpers
Helpers provide some methods, that can be reused in every file

## Controllers
Controllers call Services to get response data, that will be sent as JSON in response Middleware

## Middlewares
Middlewares provide some validation tasks, e.g. checking if user is authenticated or user is admin

## Validators
Validators are schemas, that will be activated by the validate Middleware

## Routers
Routers are express.Router instances, that call Middlewares and Controllers together

## Addons
Addons provide useful functions, so we call one function instead of connecting to DB in 'server.js' file

## Configurations
Configuration files provide constants e.g. database URI or API credentials

# DB Schema

[Diagram](https://drive.google.com/file/d/1QuGKaxS7cq33lsVY2Uw4CU_nFllapxHj/view?usp=sharing)

[Diagram (in editor)](https://dbdiagram.io/d/5f1db7c3e586385b4ff7f3f2)
