Data folder will contain models and repositories, that will be used in services.

Example structure:
 * **data**
    * **models**
        * *model-name*.js
    * **repositories**
        * *model-name*.repo.js
