Repositories folder will contain repositories, that will call database

Example structure:
 * **repositories**
    * user.repository.js
    * invoice.repository.js
    * abonement.repository.js
    * equipmentItem.repository.js
    * invoiceItem.repository.js
    * rentStatus.repository.js
    * article.repository.js
    * achievement.repository.js
    * barItem.repository.js
    * barIngredient.repository.js
    * lakeRoute.repository.js
    * beach.repository.js
    * clientService.repository.js
    * promoCode.repository.js
    * action.repository.js
    * paymentWay.repository.js
