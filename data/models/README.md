Here will be some models, to describe database structure

Example structure
 * **models**
    * user.js
    * invoice.js
    * abonement.js
    * equipmentItem.js
    * invoiceItem.js
    * rentStatus.js
    * article.js
    * achievement.js
    * barItem.js
    * barIngredient.js
    * lakeRoute.js
    * beach.js
    * clientService.js
    * promoCode.js
    * paymentWay.js
