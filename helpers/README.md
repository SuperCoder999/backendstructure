Helpers will make requests to third-party APIs (e.g. emails API, Amazon S3, QR code API, payment APIs, ...)

They will use config (API URLs)

Files:
 * email.helper.js
 * media.helper.js
 * payment.helper.js
 * qrcode.helper.js
 * pdf.helper.js
